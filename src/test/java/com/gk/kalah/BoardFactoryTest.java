package com.gk.kalah;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class BoardFactoryTest {
    @Mock
    private PitFactory pitFactory;
    @InjectMocks
    private BoardFactory boardFactory;

    @Test
    void shouldCallRepository() {
//        given
        int boardSize = 14;
        int numberOfPlayers = 2;
        int numberOfSeeds = 6;
//        when
        boardFactory.init(boardSize, numberOfPlayers, numberOfSeeds);
//        then
        verify(pitFactory).initPitList(boardSize, numberOfPlayers, numberOfSeeds);
    }
}