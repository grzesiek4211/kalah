package com.gk.kalah;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

class BoardTest {

    private final static int BOARD_SIZE = 6;
    private final static Player ANY_PLAYER_1 = Player.builder(0).build();
    private final static Player ANY_PLAYER_2 = Player.builder(1).build();
    private final static Board ANY_BOARD = new Board(BOARD_SIZE, 2, List.of());

    @ParameterizedTest
    @ValueSource(ints = {-1, BOARD_SIZE})
    void shouldThrowExceptionWhenProvideOutOfRangeIndex(int index) {
//        given
//        when
//        then
        Assertions.assertThrows(IndexOutOfBoundsException.class,
                () -> ANY_BOARD.getPit(index));
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, BOARD_SIZE})
    void shouldThrowExceptionWhenProvideOutOfRangePitId(int pitId) {
//        given
//        when
//        then
        Assertions.assertThrows(IndexOutOfBoundsException.class,
                () -> ANY_BOARD.player(pitId));
    }

    @Test
    void shouldCalculateNumberOfSeedInEachKalah() {
//        given
        List<Pit> pits = List.of(
                Pit.initHouse(1, ANY_PLAYER_1),
                Pit.initHouse(2, ANY_PLAYER_1),
                initKalahWithValue(ANY_PLAYER_1, 2),
                Pit.initHouse(0, ANY_PLAYER_2),
                Pit.initHouse(0, ANY_PLAYER_2),
                initKalahWithValue(ANY_PLAYER_1, 3));
        List<Pit> expectedPits = List.of(
                Pit.initHouse(0, ANY_PLAYER_1),
                Pit.initHouse(0, ANY_PLAYER_1),
                initKalahWithValue(ANY_PLAYER_1, 5),
                Pit.initHouse(0, ANY_PLAYER_2),
                Pit.initHouse(0, ANY_PLAYER_2),
                initKalahWithValue(ANY_PLAYER_1, 3));
        Board board = new Board(6, 2, pits);
//        when
        board.end();
//        then
        Assertions.assertIterableEquals(expectedPits, board.getPits());
    }

    private Pit initKalahWithValue(Player player, int value) {
        Pit pit = Pit.initKalah(player);
        pit.add(value);
        return pit;
    }
}