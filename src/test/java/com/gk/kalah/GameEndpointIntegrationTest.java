package com.gk.kalah;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = { KalahConfiguration.class, KalahApplication.class })
@WebAppConfiguration
class GameEndpointIntegrationTest {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @BeforeEach
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void shouldProvideGameEndpoint() {
        ServletContext servletContext = wac.getServletContext();

        Assertions.assertNotNull(servletContext);
        assertTrue(servletContext instanceof MockServletContext);
        Assertions.assertNotNull(wac.getBean("gameEndpoint"));
    }

    @Test
    public void shouldReturnCreatedGameViewWhenCallCreateGame() throws Exception {
        this.mockMvc.perform(post("/games"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.url").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
    }

    @Test
    public void shouldReturnExpectedStatusForSamplePlay() throws Exception {
        // create game
        MvcResult mvcResult = this.mockMvc.perform(post("/games"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.url").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andReturn();

        String id = JsonPath.read(mvcResult.getResponse().getContentAsString(), "$.id");
        String url = JsonPath.read(mvcResult.getResponse().getContentAsString(), "$.url");

        // make first move
        MvcResult mvcResult1 = move(id, url, 1);
        assertExpectedAndActualStatus(mvcResult1, 0,7,7,7,7,7,1,6,6,6,6,6,6,0);

        // make second move
        MvcResult mvcResult2 = move(id, url, 2);
        assertExpectedAndActualStatus(mvcResult2, 0,0,8,8,8,8,2,7,7,6,6,6,6,0);

        // make third move
        MvcResult mvcResult3 = move(id, url, 8);
        assertExpectedAndActualStatus(mvcResult3, 1,0,8,8,8,8,2,0,8,7,7,7,7,1);

        // make fourth move
        MvcResult mvcResult4 = move(id, url, 1);
        assertExpectedAndActualStatus(mvcResult4, 0,0,8,8,8,8,10,0,8,7,7,0,7,1);
    }

    private void assertExpectedAndActualStatus(MvcResult mvcResult, int ... expectedValues) throws UnsupportedEncodingException, com.fasterxml.jackson.core.JsonProcessingException {
        String json = mvcResult.getResponse().getContentAsString();
        Map<String, String> actualStatus = new ObjectMapper().readValue(json, GameView.class).getStatus();
        Map<String, String> expectedStatus = gameStatus(actualStatus.size(), expectedValues);
        Assertions.assertEquals(expectedStatus, actualStatus);
    }

    private MvcResult move(String gameId, String url, int pitId) throws Exception {
        MvcResult mvcResult;
        mvcResult = this.mockMvc.perform(put("/games/{gameId}/pits/{pitId}", gameId, pitId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.url").value(url))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(gameId))
                .andExpect(MockMvcResultMatchers.jsonPath("$.status").isNotEmpty())
                .andReturn();
        return mvcResult;
    }

    private Map<String, String> gameStatus(int size, int ... values) {
        if(size != values.length) {
            throw new IllegalArgumentException("vales.length should be equals size");
        }
        Map<String, String> map = new HashMap<>();
        for(int i = 1; i <= size; i++) {
            map.put(String.valueOf(i), String.valueOf(values[i - 1]));
        }

        return map;
    }

    @Test
    public void shouldThrowExceptionWhenWrongPlayerTryMakeMove() throws Exception {
        // create game
        MvcResult mvcResult = this.mockMvc.perform(post("/games"))
                .andExpect(status().isCreated())
                .andReturn();

        String id = JsonPath.read(mvcResult.getResponse().getContentAsString(), "$.id");
        // make first move
        this.mockMvc.perform(put("/games/{gameId}/pits/{pitId}",id, 1))
                .andExpect(status().isOk())
                .andReturn();

        // make second move (forbidden, because it's still the first player's turn)
        boolean isError = false;
        try {
            this.mockMvc.perform(put("/games/{gameId}/pits/{pitId}", id, 10)); // fixme: prepare correct error handling in code!!
        } catch (Exception e) {
            isError = true;
        }
        if(!isError) {
            Assertions.fail();
        }
    }
}