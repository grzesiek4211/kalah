package com.gk.kalah;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

class GameProcessorTest {

    private final static Player ANY_PLAYER_1 = Player.builder(0).build();
    private final static Player ANY_PLAYER_2 = Player.builder(1).build();
    private final static List<Pit> ANY_PITS = List.of(
            Pit.initHouse(0, ANY_PLAYER_1),
            Pit.initHouse(6, ANY_PLAYER_1),
            Pit.initKalah(ANY_PLAYER_1),
            Pit.initHouse(6, ANY_PLAYER_2),
            Pit.initHouse(6, ANY_PLAYER_2),
            Pit.initKalah(ANY_PLAYER_2));
    private final static int BOARD_SIZE = 6;
    private final static Board ANY_BOARD = new Board(BOARD_SIZE, 2, ANY_PITS);

    private GameProcessor gameProcessor = new GameProcessor();

    @ParameterizedTest
    @ValueSource(ints = {-1, BOARD_SIZE})
    void shouldThrowExceptionWhenProvideOutOfRangePitId(int pitId) {
//        given
//        when
//        then
        Assertions.assertThrows(IndexOutOfBoundsException.class,
                () -> gameProcessor.move(ANY_BOARD, pitId, ANY_PLAYER_1));
    }

    @Test
    void shouldThrowExceptionWhenWrongPlayerTryMakeMove() {
//        given
        int pitId = ANY_BOARD.getNumberOfPitsPerPlayer();
//        when
//        then
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> gameProcessor.move(ANY_BOARD, pitId, ANY_PLAYER_1));
    }

    @Test
    void shouldThrowExceptionWhenPlayerChooseKalah() {
//        given
        int pitId = ANY_BOARD.getNumberOfPitsPerPlayer() - 1;
//        when
//        then
        Assertions.assertThrows(IllegalStateException.class,
                () -> gameProcessor.move(ANY_BOARD, pitId, ANY_PLAYER_1));
    }

    @Test
    void shouldThrowExceptionWhenPlayerChooseEmptyPit() {
//        given
        int pitIdOfEmptyPit = 0;
//        when
//        then
        Assertions.assertThrows(IllegalStateException.class,
                () -> gameProcessor.move(ANY_BOARD, pitIdOfEmptyPit, ANY_PLAYER_1));
    }


    @Test
    void shouldReturnThisSamePlayerWhenLastPitWasPlayersKalah() {
//        given
        List<Pit> actualPits = List.of(
                Pit.initHouse(3, ANY_PLAYER_1),
                Pit.initHouse(6, ANY_PLAYER_1),
                Pit.initKalah(ANY_PLAYER_1),
                Pit.initHouse(3, ANY_PLAYER_2),
                Pit.initHouse(3, ANY_PLAYER_2),
                Pit.initKalah(ANY_PLAYER_2));
        Board board = new Board(6, 2, actualPits);
        List<Pit> expectedPits = List.of(
                Pit.initHouse(4, ANY_PLAYER_1),
                Pit.initHouse(1, ANY_PLAYER_1),
                initKalahWithValue(ANY_PLAYER_1, 2),
                Pit.initHouse(4, ANY_PLAYER_2),
                Pit.initHouse(4, ANY_PLAYER_2),
                Pit.initKalah(ANY_PLAYER_2));
        int pitId = 1;
//        when
        Player whoNext = gameProcessor.move(board, pitId, ANY_PLAYER_1);
//        then
        Assertions.assertIterableEquals(expectedPits, board.getPits());
        Assertions.assertEquals(ANY_PLAYER_1, whoNext);
    }

    @Test
    void shouldReturnNextPlayerWhenTakeStonesFromPlayers() {
//        given
        List<Pit> actualPits = List.of(
                Pit.initHouse(1, ANY_PLAYER_1),
                Pit.initHouse(0, ANY_PLAYER_1),
                Pit.initKalah(ANY_PLAYER_1),
                Pit.initHouse(3, ANY_PLAYER_2),
                Pit.initHouse(3, ANY_PLAYER_2),
                Pit.initKalah(ANY_PLAYER_2));
        Board board = new Board(6, 2, actualPits);
        List<Pit> expectedPits = List.of(
                Pit.initHouse(0, ANY_PLAYER_1),
                Pit.initHouse(0, ANY_PLAYER_1),
                initKalahWithValue(ANY_PLAYER_1, 4),
                Pit.initHouse(0, ANY_PLAYER_2),
                Pit.initHouse(3, ANY_PLAYER_2),
                Pit.initKalah(ANY_PLAYER_2));
        int pitId = 0;
//        when
        Player whoNext = gameProcessor.move(board, pitId, ANY_PLAYER_1);
//        then
        Assertions.assertIterableEquals(expectedPits, board.getPits());
        Assertions.assertEquals(ANY_PLAYER_2, whoNext);
    }

    Pit initKalahWithValue(Player player, int value) {
        Pit pit = Pit.initKalah(player);
        pit.add(value);
        return pit;
    }
}