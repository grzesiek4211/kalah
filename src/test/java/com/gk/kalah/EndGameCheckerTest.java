package com.gk.kalah;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class EndGameCheckerTest {

    private final static Player ANY_PLAYER_1 = Player.builder(0).build();
    private final static Player ANY_PLAYER_2 = Player.builder(1).build();

    private final static EndGameChecker endGameChecker = new EndGameChecker();

    @Test
    void shouldReturnTrueIfOneOfPlayersHaveAllEmptyHouses() {
//        given
        List<Pit> pits = List.of(
                Pit.initHouse(0, ANY_PLAYER_1),
                Pit.initHouse(2, ANY_PLAYER_1),
                Pit.initKalah(ANY_PLAYER_1),
                Pit.initHouse(0, ANY_PLAYER_2),
                Pit.initHouse(0, ANY_PLAYER_2),
                Pit.initKalah(ANY_PLAYER_2));
        Board board = new Board(pits.size(), 2, pits);
//        when
        boolean isEnd = endGameChecker.isEnd(board);
//        then
        Assertions.assertTrue(isEnd);
    }

    @Test
    void shouldReturnFalseIfNoPlayersHaveAllEmptyHouses() {
//        given
        List<Pit> pits = List.of(
                Pit.initHouse(0, ANY_PLAYER_1),
                Pit.initHouse(2, ANY_PLAYER_1),
                Pit.initKalah(ANY_PLAYER_1),
                Pit.initHouse(1, ANY_PLAYER_2),
                Pit.initHouse(0, ANY_PLAYER_2),
                Pit.initKalah(ANY_PLAYER_2));
        Board board = new Board(6, 2, pits);
//        when
        boolean isEnd = endGameChecker.isEnd(board);
//        then
        Assertions.assertFalse(isEnd);
    }

}