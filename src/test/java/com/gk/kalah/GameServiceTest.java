package com.gk.kalah;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class GameServiceTest {
    private static List<Pit> ANY_PITS = List.of(
            Pit.initHouse(6, Player.builder(1).build()),
            Pit.initHouse(6, Player.builder(1).build()),
            Pit.initKalah(Player.builder(1).build()),
            Pit.initHouse(6, Player.builder(2).build()),
            Pit.initHouse(6, Player.builder(2).build()),
            Pit.initKalah(Player.builder(2).build()));
    private final static int BOARD_SIZE = 6;
    private static Board ANY_BOARD = new Board(BOARD_SIZE, 2, ANY_PITS);
    private static Player ANY_PLAYER = Player.builder(1).build();
    private static Player ANY_PLAYER_2 = Player.builder(2).build();
    private static Game ANY_GAME = Game.builder(ANY_BOARD, ANY_PLAYER).id(1L).build();
    @Mock
    private GameRepository gameRepository;
    @Mock
    private GameFactory gameFactory;
    @Mock
    private GameProcessor gameProcessor;
    @Mock
    private EndGameChecker endGameChecker;
    @InjectMocks
    private GameService gameService;

    @Test
    void shouldCallRepository() {
//        given
        int boardSize = 14;
        int numberOfPlayers = 2;
        int numberOfSeeds = 6;
//        when
        gameService.create(boardSize, numberOfPlayers, numberOfSeeds);
//        then
        verify(gameFactory).init(boardSize, numberOfPlayers, numberOfSeeds);
        verify(gameRepository).save(ArgumentMatchers.any());
    }

    @Test
    void shouldThrowExceptionWhenGameDoesNotExist() {
//        given
        long gameId = 1L;
        int pitId = 1;
//        when
        when(gameRepository.findById(gameId)).thenReturn(Optional.empty());
//        then
        Assertions.assertThrows(NoSuchElementException.class,
                () -> gameService.move(gameId, pitId));
    }

    @Test
    void shouldMakeMoveAndSave() {
//        given
        long gameId = 1L;
        int pitId = 3;
//        when
        when(gameRepository.findById(gameId)).thenReturn(Optional.of(ANY_GAME));
        when(endGameChecker.isEnd(ANY_BOARD)).thenReturn(true);
        when(gameRepository.save(ANY_GAME)).thenReturn(ANY_GAME);
        when(gameProcessor.move(ANY_BOARD, pitId, ANY_GAME.getWhoseTurn())).thenReturn(ANY_PLAYER_2);
        Game updatedGame = gameService.move(gameId, pitId);
//        then
        verify(gameRepository).findById(gameId);
        verify(endGameChecker).isEnd(ANY_BOARD);
        verify(gameProcessor).move(ANY_BOARD, pitId, ANY_PLAYER);
        verify(gameRepository).save(ANY_GAME);
        Assertions.assertEquals(updatedGame.getWhoseTurn(), ANY_PLAYER_2);
    }
}