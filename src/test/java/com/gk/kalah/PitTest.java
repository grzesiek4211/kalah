package com.gk.kalah;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PitTest {

    @Test
    void shouldReturnTrueWhenPitIsEmpty() {
//        given
        final Pit pit = Pit.initHouse(0, Player.builder(1).build());
//        when
        boolean isEmpty = pit.isEmpty();
//        then
        assertTrue(isEmpty);
    }

    @Test
    void shouldReturnFalseWhenPitIsEmpty() {
//        given
        final Pit pit = Pit.initHouse(1, Player.builder(1).build());
//        when
        boolean isEmpty = pit.isEmpty();
//        then
        assertFalse(isEmpty);
    }

    @Test
    void shouldReturnTrueWhenPitContainsOneSeed() {
//        given
        final Pit pit = Pit.initHouse(1, Player.builder(1).build());
//        when
        boolean containsOneSeed = pit.containsOneSeed();
//        then
        assertTrue(containsOneSeed);
    }

    @Test
    void shouldReturnFalseWhenPitDoNotContains() {
//        given
        final Pit pit = Pit.initHouse(0, Player.builder(1).build());
//        when
        boolean containsOneSeed = pit.containsOneSeed();
//        then
        assertFalse(containsOneSeed);
    }

    @Test
    void shouldBeEmptyAfterCall() {
//        given
        int numberOfSeeds = 1;
        final Pit pit = Pit.initHouse(numberOfSeeds, Player.builder(1).build());
//        when
        int actualNumberOfSeeds = pit.empty();
//        then
        assertTrue(pit.isEmpty());
        assertEquals(numberOfSeeds, actualNumberOfSeeds);
    }

    @Test
    void shouldNotAddWhenPlayerIsNotOwnerOfKalah() {
//        given
        final Pit pit = Pit.initKalah(Player.builder(1).build());
        final int expectedValue = pit.getValue();
        final Player whoMakesMove = Player.builder(2).build();
//        when
        final int actualValue = pit.tryAdd(whoMakesMove);
//        then
        assertEquals(expectedValue, actualValue);
    }

    @Test
    void shouldAddWhenPlayerIsOwnerOfKalah() {
//        given
        final Player whoMakesMove = Player.builder(1).build();
        final Pit pit = Pit.initKalah(whoMakesMove);
        final int initialValue = pit.getValue();
//        when
        final int actualValue = pit.tryAdd(whoMakesMove);
//        then
        assertTrue(initialValue < actualValue);
    }

    @Test
    void shouldAddWhenPlayerIsOwnerOfHouse() {
//        given
        final Player whoMakesMove = Player.builder(1).build();
        final Pit pit = Pit.initHouse(0, whoMakesMove);
        final int initialValue = pit.getValue();
//        when
        final int actualValue = pit.tryAdd(whoMakesMove);
//        then
        assertTrue(initialValue < actualValue);
    }

    @Test
    void shouldAddWhenPlayerIsNotOwnerOfHouse() {
//        given
        final Pit pit = Pit.initHouse(0, Player.builder(1).build());
        final int initialValue = pit.getValue();
        final Player whoMakesMove = Player.builder(2).build();
//        when
        final int actualValue = pit.tryAdd(whoMakesMove);
//        then
        assertTrue(initialValue < actualValue);
    }
}