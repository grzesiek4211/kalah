package com.gk.kalah;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class GameFactoryTest {
    @Mock
    private BoardFactory boardFactory;
    @InjectMocks
    private GameFactory gameFactory;

    @Test
    void shouldCallRepository() {
//        given
        int boardSize = 14;
        int numberOfPlayers = 2;
        int numberOfSeeds = 6;
//        when
        gameFactory.init(boardSize, numberOfPlayers, numberOfSeeds);
//        then
        verify(boardFactory).init(boardSize, numberOfPlayers, numberOfSeeds);
    }
}