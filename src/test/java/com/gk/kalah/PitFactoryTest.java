package com.gk.kalah;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.List;

class PitFactoryTest {

    PitFactory pitFactory = new PitFactory();

    @ParameterizedTest
    @CsvSource({"0,2,2", "2,0,2", "2,2,0", "0,0,0"})
    void shouldBeImpossiblePassLessThan1Param(int boardSize, int numberOfPlayers, int numberOfSeeds) {
//        given
//        when
//        then
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> pitFactory.initPitList(boardSize, numberOfPlayers, numberOfSeeds));
    }

    @Test
    void shouldCreatedCorrectListForTwoPlayers() {
//        given
        int boardSize = 6;
        int numberOfPlayers = 2;
        int numberOfSeeds = 6;
//        when
        List<Pit> pits = pitFactory.initPitList(boardSize, numberOfPlayers, numberOfSeeds);
//        then
        List<Pit> expectedResult = List.of(
                Pit.initHouse(numberOfSeeds, Player.builder(0).build()),
                Pit.initHouse(numberOfSeeds, Player.builder(0).build()),
                Pit.initKalah(Player.builder(0).build()),
                Pit.initHouse(numberOfSeeds, Player.builder(1).build()),
                Pit.initHouse(numberOfSeeds, Player.builder(1).build()),
                Pit.initKalah(Player.builder(1).build()));
        Assertions.assertIterableEquals(expectedResult, pits);
    }

    @Test
    void shouldCreatedCorrectListForMoreThanTwoPlayers() {
//        given
        int boardSize = 9;
        int numberOfPlayers = 3;
        int numberOfSeeds = 4;
//        when
        List<Pit> pits = pitFactory.initPitList(boardSize, numberOfPlayers, numberOfSeeds);
//        then
        List<Pit> expectedResult = List.of(Pit.initHouse(numberOfSeeds, Player.builder(0).build()),
                Pit.initHouse(numberOfSeeds, Player.builder(0).build()),
                Pit.initKalah(Player.builder(0).build()),
                Pit.initHouse(numberOfSeeds, Player.builder(1).build()),
                Pit.initHouse(numberOfSeeds, Player.builder(1).build()),
                Pit.initKalah(Player.builder(1).build()),
                Pit.initHouse(numberOfSeeds, Player.builder(2).build()),
                Pit.initHouse(numberOfSeeds, Player.builder(2).build()),
                Pit.initKalah(Player.builder(2).build()));
        Assertions.assertIterableEquals(expectedResult, pits);
    }
}