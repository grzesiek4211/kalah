package com.gk.kalah;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(builderMethodName = "hiddenBuilder")
class Player {
    int id;

    public static PlayerBuilder builder(int id) {
        return hiddenBuilder().id(id);
    }
}
