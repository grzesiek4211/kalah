package com.gk.kalah;

import lombok.Value;
import lombok.val;

import java.io.Serializable;
import java.util.List;

@Value
class Board implements Serializable {
    int size;
    int numberOfPlayers;
    List<Pit> pits;

    public Pit getPit(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException(String.format("Index [%d] is out of range 0-%d", index, size));
        }

        return pits.get(index);
    }

    public Player player(int pitId) {
        if (pitId < 0 || pitId >= size) {
            throw new IndexOutOfBoundsException(String.format("Index [%d] is out of range 0-%d", pitId, size));
        }
        val numberOfPitsPerPlayer = getNumberOfPitsPerPlayer();
        int playerId = pitId / numberOfPitsPerPlayer;

        return Player.builder(playerId).build();
    }

    public int getNumberOfPitsPerPlayer() {
        return size / numberOfPlayers;
    }

    public int takeStonesFromPlayers(int pitId) {
        if (numberOfPlayers > 2) {
            throw new UnsupportedOperationException("Not implemented yet"); // fixme: find rules on the Internet and implement
        }
        val kalah = getKalah(player(pitId));
        int oppositeIndex = numberOfPlayers * (getNumberOfPitsPerPlayer() - 1) - pitId;
        int index = numberOfPlayers * (getNumberOfPitsPerPlayer() - 1) - oppositeIndex;
        int sum = pits.get(oppositeIndex).empty() + pits.get(index).empty();

        return kalah.add(sum);
    }

    private Pit getKalah(Player player) {
        return pits.get(getNumberOfPitsPerPlayer() * (player.getId() + 1) - 1);
    }

    public void end() {
        var sum = 0;
        for(Pit pit: pits) {
            if(pit.isKalah()) {
                pit.add(sum);
                sum = 0;
                continue;
            }
            sum += pit.empty();
        }
    }
}
