package com.gk.kalah;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.io.Serializable;

@Getter
@EqualsAndHashCode
@AllArgsConstructor(access = AccessLevel.PRIVATE)
class Pit implements Serializable {
    private final Player player;
    private final boolean isKalah;
    private int value;

    public static Pit initHouse(int value, Player player) {
        return new Pit(player, false, value);
    }

    public static Pit initKalah(Player player) {
        return new Pit(player, true, 0);
    }

    public boolean isEmpty() {
        return value == 0;
    }

    public boolean isOwner(Player whoMakeMove) {
        return player.equals(whoMakeMove);
    }

    public boolean containsOneSeed() {
        return value == 1;
    }

    public int empty() {
        int numberOfSeeds = value;
        value = 0;
        return numberOfSeeds;
    }

    public int tryAdd(Player whoMakeMove) {
        if (isKalah && !player.equals(whoMakeMove)) {
            return value;
        }

        return ++value;
    }

    public int add(int number) {
        return value += number;
    }
}
