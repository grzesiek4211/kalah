package com.gk.kalah;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
class GameFactory {
    private final BoardFactory boardFactory;

    Game init(int boardSize, int numberOfPlayers, int numberOfSeeds) {
        return Game.builder(boardFactory.init(boardSize, numberOfPlayers, numberOfSeeds), Player.builder(0).build())
                .build();
    }
}
