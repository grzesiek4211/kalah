package com.gk.kalah;

import org.springframework.stereotype.Component;

@Component
class EndGameChecker {
    public boolean isEnd(Board board) {
        var playerWithEmptyPitsExists = false;
        int iter = 0;
        int numOfPlayers = board.getNumberOfPitsPerPlayer();
        while (!playerWithEmptyPitsExists && iter < board.getNumberOfPlayers()) {
            playerWithEmptyPitsExists = true;
            for (int i = iter * numOfPlayers; i < iter * numOfPlayers + numOfPlayers; i++) {
                if (!board.getPit(i).isKalah() && !board.getPit(i).isEmpty()) {
                    playerWithEmptyPitsExists = false;
                    break;
                }
            }
            iter++;
        }
        return playerWithEmptyPitsExists;
    }
}
