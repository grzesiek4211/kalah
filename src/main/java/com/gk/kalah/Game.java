package com.gk.kalah;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@EqualsAndHashCode
@Builder(builderMethodName = "hiddenBuilder")
class Game implements Serializable {
    private final Long id;
    private final Board board;
    @Setter
    private Player whoseTurn;

    public static GameBuilder builder(Board board, Player whoseTurn) {
        return hiddenBuilder().board(board).whoseTurn(whoseTurn);
    }

    public void end() {
        board.end();
    }
}