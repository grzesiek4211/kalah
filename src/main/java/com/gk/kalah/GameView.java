package com.gk.kalah;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
public class GameView implements Serializable {
    private String id;
    private String url;
    private Map<String, String> status;
}