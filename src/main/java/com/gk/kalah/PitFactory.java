package com.gk.kalah;

import lombok.val;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
class PitFactory {
    public List<Pit> initPitList(int boardSize, int numberOfPlayers, int numberOfSeeds) {
        if (boardSize <= 0 || numberOfPlayers <= 0 || numberOfSeeds <= 0) {
            throw new IllegalArgumentException(String.format("Params should be positive numbers, [boardSize = %d]; " +
                    "[numberOfPlayers = %d];  [numberOfSeeds = %d]", boardSize, numberOfPlayers, numberOfSeeds));
        }
        List<Pit> result = new ArrayList<>(boardSize);
        int numberOfPitsPerPlayer = boardSize / numberOfPlayers;
        val kalahPosition = numberOfPitsPerPlayer - 1;

        for (int i = 0; i < boardSize; i++) {
            if (i % numberOfPitsPerPlayer != kalahPosition) {
                result.add(Pit.initHouse(numberOfSeeds, player(i, numberOfPitsPerPlayer)));
            } else {
                result.add(Pit.initKalah(player(i, numberOfPitsPerPlayer)));
            }
        }
        return result;
    }

    private Player player(int iter, int numberOfPitsPerPlayer) {
        return Player.builder(playerId(iter, numberOfPitsPerPlayer)).build();
    }

    private int playerId(int iter, int numberOfPitsPerPlayer) {
        return iter / numberOfPitsPerPlayer;
    }

}
