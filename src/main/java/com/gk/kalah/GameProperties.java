package com.gk.kalah;

import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@Value
@AllArgsConstructor(onConstructor = @__({@ConstructorBinding}))
@ConfigurationProperties(prefix = "game.rules")
class GameProperties implements GamePolicy {
    int numberOfPlayer;
    int boardSize;
    int numberOfSeeds;
}
