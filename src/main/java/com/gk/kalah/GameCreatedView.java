package com.gk.kalah;

import lombok.Data;

import java.io.Serializable;

@Data
public class GameCreatedView implements Serializable {
    String id;
    String url;
}