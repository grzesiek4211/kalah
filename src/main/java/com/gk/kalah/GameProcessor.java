package com.gk.kalah;

import lombok.val;
import org.springframework.stereotype.Service;

@Service
class GameProcessor {

    public Player move(Board board, Integer pitId, Player whoseTurn) {
        BoardValidator.isMovementPossible(board, pitId, whoseTurn);

        int lastIndex = takeATurn(board, whoseTurn, pitId);
        Pit lastPit = board.getPit(lastIndex);

        if (lastPit.isOwner(whoseTurn) && lastPit.isKalah()) {
            return whoseTurn;
        }

        if (lastPit.isOwner(whoseTurn) && lastPit.containsOneSeed()) {
            board.takeStonesFromPlayers(lastIndex);
        }
        return nextPlayer(board, whoseTurn);
    }

    private int takeATurn(Board board, Player whoseTurn, int index) {
        Pit pit = board.getPit(index);
        int numberOfSeeds = pit.getValue();
        pit.empty();
        for (int i = numberOfSeeds; i > 0; ) {
            index = nextIndex(board, index);
            Pit actualPit = board.getPit(index);
            if (actualPit.getValue() < actualPit.tryAdd(whoseTurn)) {
                i--;
            }
        }
        return index;
    }

    private int nextIndex(Board board, int index) {
        if (index == board.getSize() - 1) {
            return 0;
        }

        return index + 1;
    }

    private Player nextPlayer(Board board, Player whoMadeMove) {
        if (whoMadeMove.getId() == board.getNumberOfPlayers() - 1) {
            return Player.builder(0).build();
        }

        return Player.builder(whoMadeMove.getId() + 1).build();
    }

    private static class BoardValidator {
        static void isMovementPossible(Board board, int pitId, Player whoseTurn) {
            if (pitId < 0 || pitId > board.getSize() - 1) {
                throw new IndexOutOfBoundsException(String.format("PitId [pitId = %d] should be in the range 0-%d", pitId, board.getSize()));
            }

            if (!whoMakeMove(board, pitId).equals(whoseTurn)) {
                throw new IllegalArgumentException("It is not this player's turn");
            }

            val pit = board.getPit(pitId);
            if (pit.isKalah() || pit.isEmpty()) {
                throw new IllegalStateException("Cannot select an empty pit or Kalah");
            }
        }

        static private Player whoMakeMove(Board board, int pitId) {
            return board.player(pitId);
        }
    }
}
