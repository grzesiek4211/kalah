package com.gk.kalah;

interface GameConfigurationPolicy {
    default String getHost() {
        return "localhostaaa";
    }

    default int getPort() {
        return 8080;
    }
}
