package com.gk.kalah;

import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
class GameService {
    private final GameRepository gameRepository;
    private final GameFactory gameFactory;
    private final GameProcessor gameProcessor;
    private final EndGameChecker endGameChecker;

    public Game create(int boardSize, int numberOfPlayer, int numberOfSeeds) {
        return gameRepository.save(gameFactory.init(boardSize, numberOfPlayer, numberOfSeeds));
    }

    public Game move(long gameId, int pitId) {
        val game = gameRepository.findById(gameId).orElseThrow();
        val whoNext = gameProcessor.move(game.getBoard(), pitId, game.getWhoseTurn());
        game.setWhoseTurn(whoNext);
        if(endGameChecker.isEnd(game.getBoard())) {
            game.end();
        }
        return gameRepository.save(game);
    }
}
