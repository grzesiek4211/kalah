package com.gk.kalah;

import lombok.AllArgsConstructor;
import lombok.val;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/games")
@AllArgsConstructor
public class GameEndpoint {
    private final GameService gameService;
    private final GameMapper gameMapper;
    private final GamePolicy gamePolicy;
    private final GameConfigurationPolicy gameConfigurationPolicy;

    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<GameCreatedView> createGame() {
        val game = gameService.create(gamePolicy.getBoardSize(), gamePolicy.getNumberOfPlayer(), gamePolicy.getNumberOfSeeds());
        return ResponseEntity.status(201).body(gameMapper.gameCreatedView(game, gameConfigurationPolicy));
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "application/json", path = "/{gameId}/pits/{pitId}")
    public ResponseEntity<GameView> move(@PathVariable Long gameId, @PathVariable Integer pitId) {
        val game = gameService.move(gameId, gamePolicy.getBackendPitId(pitId));
        return ResponseEntity.ok(gameMapper.gameView(game, gamePolicy, gameConfigurationPolicy));
    }
}