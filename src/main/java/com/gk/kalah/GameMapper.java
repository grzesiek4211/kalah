package com.gk.kalah;

import org.mapstruct.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Mapper(componentModel = "spring")
        //Creates a Spring Bean automatically
interface GameMapper {

    @Mapping(target = "url", ignore = true)
    GameCreatedView gameCreatedView(Game game, @Context GameConfigurationPolicy gameConfigurationPolicy);

    @AfterMapping
    default void updateGameCreatedView(@MappingTarget GameCreatedView result, @Context GameConfigurationPolicy gameConfigurationPolicy) {
        result.setUrl(String.format("http://%s:%d/games/%s",
                gameConfigurationPolicy.getHost(),
                gameConfigurationPolicy.getPort(),
                result.getId()));
    }

    @Mapping(target = "url", ignore = true)
    @Mapping(target = "status", source = "board.pits")
    GameView gameView(Game game, @Context GamePolicy gamePolicy, @Context GameConfigurationPolicy gameConfigurationPolicy);

    default Map<String, String> status(List<Pit> pits, @Context GamePolicy gamePolicy) {
        if (pits == null) {
            return null;
        }

        Map<String, String> status = new HashMap<>();
        for (int i = 0; i < pits.size(); i++) {
            status.put(String.valueOf(gamePolicy.getFrontPitId(i)), String.valueOf(pits.get(i).getValue()));
        }
        return status;
    }

    @AfterMapping
    default void updateGameView(@MappingTarget GameView result, @Context GameConfigurationPolicy gameConfigurationPolicy) {
        result.setUrl(String.format("http://%s:%d/games/%s",
                gameConfigurationPolicy.getHost(),
                gameConfigurationPolicy.getPort(),
                result.getId()));
    }
}
