package com.gk.kalah;

import java.util.Optional;

interface GameRepository {
    Optional<Game> findById(Long id);

    Game save(Game game);
}
