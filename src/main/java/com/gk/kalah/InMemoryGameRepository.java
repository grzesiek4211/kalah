package com.gk.kalah;

import lombok.Builder;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
class InMemoryGameRepository implements GameRepository {
    private Map<Long, GameEntity> inMemoryDB = new HashMap<>();
    private Long sizeDB = 0L;

    @Override
    public Optional<Game> findById(Long id) {
        return Optional.of(GameMapper.game(inMemoryDB.get(id)));
    }

    @Override
    public Game save(Game game) {
        if (!inMemoryDB.containsKey(game.getId())) {
            sizeDB++;
        }
        GameEntity entity = GameMapper.gameEntity(game, sizeDB);
        inMemoryDB.put(entity.id, entity);
        return GameMapper.game(entity);
    }

    @Builder
    private static class GameEntity {
        Long id;
        Board board;
        Player whoseTurn;
    }

    private static class GameMapper {
        static Game game(GameEntity gameEntity) {
            if (gameEntity == null) {
                return null;
            }

            return Game.builder(gameEntity.board, gameEntity.whoseTurn)
                    .id(gameEntity.id)
                    .build();
        }

        static GameEntity gameEntity(Game game, Long id) {
            if (game == null || id == null) {
                return null;
            }

            return GameEntity.builder()
                    .id(game.getId() == null ? id : game.getId())
                    .whoseTurn(game.getWhoseTurn())
                    .board(game.getBoard())
                    .build();
        }
    }
}
