package com.gk.kalah;

interface GamePolicy {
    default int getBackendPitId(int frontPitId) {
        return frontPitId - 1;
    }

    default int getFrontPitId(int backendPitId) {
        return backendPitId + 1;
    }

    default int getBoardSize() {
        return 14;
    }

    default int getNumberOfPlayer() {
        return 2;
    }

    default int getNumberOfSeeds() {
        return 6;
    }
}
