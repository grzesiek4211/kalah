package com.gk.kalah;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
class BoardFactory {
    private final PitFactory pitFactory;

    public Board init(int boardSize, int numberOfPlayers, int numberOfSeeds) {
        return new Board(boardSize, numberOfPlayers, pitFactory.initPitList(boardSize, numberOfPlayers, numberOfSeeds));
    }
}